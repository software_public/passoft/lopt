=======
History
=======

Display time in PASSCAL time format2018.137 (2018-06-07)
------------------

* First release on new build system.

2020.205 (2020-07-23)
------------------
* Updated to work with Python 3
* Added some unit tests to ensure basic functionality of lopt
* Updated list of platform specific dependencies to be installed when
  installing lopt in development mode (see setup.py)
* Installed and tested lopt against Python3.[6,7,8] using tox
* Formatted Python code to conform to the PEP8 style guide
* Created conda packages for lopt that can run on Python3.[6,7,8]
* Updated .gitlab-ci.yml to run a linter and unit tests for Python3.[6,7,8]
  in GitLab CI pipeline

2022.1.0.0 (2022-01-11)
------------------
* New versioning scheme
