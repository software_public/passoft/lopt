#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for `lopt` package."""

import unittest
import socket

from unittest.mock import patch
from lopt.lopt import main, getHostname


class TestLopt(unittest.TestCase):
    """Tests for `lopt` package."""

    def test_import(self):
        """Test lopt import"""
        with patch('sys.argv', ['lopt', '-h']):
            with self.assertRaises(SystemExit) as cmd:
                main()
            self.assertEqual(cmd.exception.code, 0, "sys.exit(0) never called "
                             "-  Failed to exercise lopt")

    def test_get_host_name(self):
        """Tests that getHostname gets the correct hostname"""
        lopt_name = getHostname()
        test_name = socket.gethostname()
        error_message = "Function getHostname didn't get the correct hostname."
        self.assertEqual(lopt_name, test_name, error_message)
