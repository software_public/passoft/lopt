====
lopt
====

* Description: Clock that displays GMT in PASSCAL time format.

* Usage: lopt

* Free software: GNU General Public License v3 (GPLv3)
