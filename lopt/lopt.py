#!/usr/bin/env python

#####################
#
# A simple clock
#
# Steve Azevedo, Oct. 2000
# updated python3 2018 Derick Hess
#####################
#
# modification
# version: 2020.205
# author: Maeva Pourpoint
#
# Updates to work under Python 3.
# Unit tests to ensure basic functionality.
# Code cleanup to conform to the PEP8 style guide.
# Directory cleanup (remove unused files introduced by Cookiecutter).
# Packaged with conda.
#####################
#
# modification
# version: 2022.1.0.0
# author: Maeva Pourpoint
#
# Update versioning scheme
#####################

import sys

from os import popen
from time import gmtime, sleep, time
from tkinter import Frame, GROOVE, Label, Tk

colors = [('black', 'white'),
          ('red', 'black'),
          ('green', 'black'),
          ('yellow', 'black'),
          ('blue', 'white'),
          ('magenta', 'black'),
          ('cyan', 'black'),
          ('white', 'black')]

fontsizes = [24, 36, 48, 64, 72, 96]


def getHostname():
    fh = popen('hostname')
    name = fh.readline()
    fh.close()
    name = name[:-1]
    return name


class Pclock(Frame):
    def __init__(self, root, **kw):
        Frame.__init__(self, root, **kw)
        # apply(Frame.__init__, (self, root), kw)
        self.getTime()
        # tzname = ('GMT', 'GMT')
        root.resizable(0, 0)
        self.configure(relief=GROOVE, borderwidth=2)
        self.icolor = 0
        self.ifont = 0
        self.label = Label(self, text=self.time,
                           background=colors[self.icolor][0],
                           foreground=colors[self.icolor][1])
        self.label.bind("<Button-1>", self.showDate)
        self.label.bind("<Button-2>", self.changeColor)
        self.label.bind("<Button-3>", self.changeSize)
        self.label.pack()

    def start(self):
        self.afterId = self.after(200, self.update)

    def getTime(self):
        t = time() + 0.5
        ttuple = gmtime(int(t))
        self.ttuple = ttuple
        self.time = ("%4d:%03d:%02d:%02d:%02d" % (int(ttuple[0]),
                                                  int(ttuple[7]),
                                                  int(ttuple[3]),
                                                  int(ttuple[4]),
                                                  int(ttuple[5])))

    def update(self):
        self.getTime()
        self.label.configure(text=self.time)
        self.update_idletasks()
        self.afterId = self.after(200, self.update)

    def showDate(self, e):
        self.after_cancel(self.afterId)
        date = ("    %d/%02d/%02d   " % (int(self.ttuple[0]),
                                         int(self.ttuple[1]),
                                         int(self.ttuple[2])))
        self.label.configure(text=date)
        self.update_idletasks()
        sleep(3)
        self.start()

    def changeSize(self, e):
        self.after_cancel(self.afterId)
        self.ifont = (self.ifont + 1) % 6
        self.label.configure(font=('Courier',
                                   fontsizes[self.ifont],
                                   'bold'))
        self.update_idletasks()
        self.start()

    def changeColor(self, e):
        self.after_cancel(self.afterId)
        self.icolor = (self.icolor + 1) % 8
        self.label.configure(background=colors[self.icolor][0],
                             foreground=colors[self.icolor][1])
        self.update_idletasks()
        self.start()


def main():
    if len(sys.argv) > 1:
        print("USAGE: lopt \nDescription: Clock that displays GMT in PASSCAL "
              "time format")
        sys.exit(0)
    root = Tk()
    hostname = getHostname()
    root.title("Livin' on PASSCAL Time: " + hostname)
    p = Pclock(root, background='white')
    p.pack()
    p.start()
    root.mainloop()


if __name__ == "__main__":
    main()
